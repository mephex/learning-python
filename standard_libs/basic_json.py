import json
"""
https://docs.python.org/3/library/json.html#basic-usage
"""

data = {
    "president": {
        "name": "Zaphod Beeblebrox",
        "species": "Betelgeusian"
    }
}

with open("data_file.json","w") as write_file:
  json.dump(data, write_file)

print(json.dumps(data))
print(json.dumps(data, indent=2))

