from email.utils import decode_params
import json

blackjack_hand = (8, "Q")
encoded_hand = json.dumps(blackjack_hand)
decoded_hand = json.loads(encoded_hand)

blackjack_hand == decoded_hand
# False
type(blackjack_hand)
# <class 'tuple'>
type(decoded_hand)
# <class 'list'>
blackjack_hand == tuple(decoded_hand)
# True
