"""
Learning to manipulate json with Python
"""
import json

with open("senators.json","r") as senator_data:
  senators = json.loads(senator_data)

print(senators)