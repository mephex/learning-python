"""
http://www.vizgr.org/historical-events/search.php?format=json&begin_date=-3000000&end_date=20151231&lang=en
"""
import json

def dict_raise_on_duplicates(ordered_pairs):
  """Reject duplicate keys."""
  d = {}
  for k, v in ordered_pairs:
    if k in d:
      raise ValueError("duplicate key: %r" % (k,))
    else:
      d[k] = v
  return d

def array_on_duplicate_keys(ordered_pairs):
  """Convert duplicate keys to arrays."""
  d = {}
  for k, v in ordered_pairs:
    if k in d:
      if type(d[k]) is list:
        d[k].append(v)
      else:
        d[k] = [d[k],v]
    else:
      d[k] = v
  return d


with open("historical-events.json","r") as historical_events:
  events = json.load(historical_events, object_pairs_hook=array_on_duplicate_keys)

  

