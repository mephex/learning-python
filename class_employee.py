class employee:
  def __init__(self, firstname, lastname, salary) -> None:
    self.firstname = firstname
    self.lastname = lastname
    self.salary = salary
    self.email = self.firstname + "." + self.lastname + "@test.com"

  def giveRaise(self, salary):
    self.salary = salary

class developer(employee):
  def __init__(self, firstname, lastname, salary, programming_languages):
    super().__init__(firstname, lastname, salary)
    self.programming_languages = programming_languages

  def add_language(self, language):
    self.programming_languages =+ [language]

employee1 = employee("Jon", "Smith", 80000)
print(employee1.salary)

employee1.giveRaise(100000)

print(employee1.salary)

dev1 = developer("Joe","Freeman", 100000, ["Python","C"])
print(dev1.programming_languages)