# List - Ordered sequence of elements that can have a variable length

stack = []
stack.append(1)
stack.append(2)
pop_elem = stack.pop()

queue = []
queue.append(1)
queue.append(2)
pop_elem = queue.pop(0)

# Dict - 

ages = dict()

ages["Bob"] = 22
ages["Emily"] = 20

for key, value in ages.items():
  print(key, value)

# Set - Iterable data stucture that only contains unique values

set1 = set([1,2,4,6])
set2 = set([1,3,5,7])

intersection = set1 & set2
print(intersection)